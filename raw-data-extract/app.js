#!/usr/bin/env node

'use strict';

const parse      = require('csv-parse');
const util       = require('util');
const fs         = require('fs');
const path       = require('path');
const mysql      = require('mysql');
const async      = require('async');
const co         = require('co');
const csvHeaders = require('csv-headers');
const leftpad    = require('leftpad');

const dbhost = 'localhost';
const dbuser = 'root';
const dbpass = '';
const dbname = 'proximityid_db';
const tblnm  = 'raw_data';
const csvfn  = 'sample_data.csv';

new Promise((resolve, reject) => {
    csvHeaders({
        file      : csvfn,
        delimiter : ','
    }, function(err, headers) {
        if (err) reject(err);
        else resolve({ headers });
    });
})
.then(context => {
    return new Promise((resolve, reject) => {

        context.db = mysql.createConnection({
            host     : dbhost,
            user     : dbuser,
            password : dbpass,
            database : dbname
        });

        context.db.connect((err) => {
            if (err) {
                console.error('error connecting: ' + err.stack);
                reject(err);
            } else {
                resolve(context);
            }
        });
    })
})
.then(context => {
    return new Promise((resolve, reject) => {
        context.db.query(`DROP TABLE IF EXISTS ${tblnm}`,
        [ ],
        err => {
            if (err) reject(err);
            else resolve(context);
        })
    });
})
.then(context => {
    return new Promise((resolve, reject) => {
        var fields = '';
        var fieldnms = '';
        var qs = '';
        context.headers.forEach(hdr => {
            hdr = hdr.replace(' ', '_');
            if (fields !== '') 
				fields += ',';
            if (fieldnms !== '') 
				fieldnms += ','
            if (qs !== '') 
				qs += ',';
			hdr = hdr.replace(';',' TEXT,');//
            fields += `${hdr} TEXT`;
			hdr = hdr.replace(';',' TEXT,');//
            fieldnms += ` ${hdr}`;
			hdr = hdr.replace(';',' TEXT,');//
            qs += ' ?';
			console.log(`${fieldnms}`);
        });
        context.qs = qs;
        context.fieldnms = fieldnms;
        console.log(`about to create CREATE TABLE IF NOT EXISTS ${tblnm} ( ${fieldnms +' TEXT'} )`);
        context.db.query(`CREATE TABLE IF NOT EXISTS ${tblnm} ( ${fieldnms +' TEXT'}  )`,
        [ ],
        err => {
            if (err) reject(err);
            else resolve(context);
        })
    });
})
.then(context => {
    return new Promise((resolve, reject) => {
        fs.createReadStream(csvfn).pipe(parse({
            delimiter: ',',
            columns: true,
            relax_column_count: true
        }, (err, data) => {
            if (err) return reject(err);
            async.eachSeries(data, (datum, next) => {
                //console.log(`about to run INSERT INTO ${tblnm} ( ${context.fieldnms} ) VALUES ( ${context.qs} )`);
                var d = [];
                try {
                    context.headers.forEach(hdr => {
                        let tp = datum[hdr]; //.trim();
                        // For a field with an empty string, send NULL instead
                        d.push(tp === '' ? null : tp);
                    });
                } catch (e) {
                    console.error(e.stack);
                }
                //console.log(`${d.length}: ${util.inspect(d)}`);
                var vals = util.inspect(datum, false, false);
				vals = vals.replace(';',',');
				vals = vals.replace(';',',');
				vals = vals.replace(';',"','");
				vals = vals.replace(';',"','");
				vals = vals.replace("'",'');
				vals = vals.replace("'",'');
				vals = vals.replace('{','');
				vals = vals.replace('}','');
                vals = vals.replace(':',' ) value (');

                var qury = context.fieldnms;
                qury = qury.replace('TEXT', '');
                qury = qury.replace('TEXT', '');
                var array = vals.split(')');
                vals = vals.replace(array[0], 'INSERT INTO ' + tblnm +' ('+qury);
                vals = vals+')';

                console.log(vals);

                if (d.length > 0) {
                    context.db.query(vals, d,
                    err => {
                        if (err) { console.error(err); next(err); }
                        else setTimeout(() => { next(); });
                    });
                } else { console.log(`empty row ${util.inspect(datum)} ${util.inspect(d)}`); next(); }
            },
            err => {
                if (err) reject(err);
                else resolve(context);
            });
        }));
    });
})
.then(context => { context.db.end(); })
.catch(err => { console.error(err.stack); });
