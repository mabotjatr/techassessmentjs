Step:1 install
-Nodejs v8.11.3
-Npm 5.6.0
-Angular 6 CLI
-xampp control panel 

Step:2 
- create a database called "proximityid_db"
- open folder name "raw-data-extract"
- from this folder run command: node app.js (to create table and extract data from csv into mysql table).
- This will time sometime to finish.
- once this script is done run command: node proocessRowData.js (to process raw data into useful data)

Step:2
- open folder name "rest-api"
- from this folder run command: node app.js (start rest-api server )

Step:3
To run the application do the following
For Windows:
Open the start menu
Type windows+R or open "Run"
Execute the following command:

chrome.exe --user-data-dir="C://Chrome dev session" --disable-web-security

once the browser is open copy and paste this url: http://localhost:4200/