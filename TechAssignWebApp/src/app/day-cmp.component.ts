import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './day-cmp.component.html',
  styleUrls: ['./day-cmp.component.css']
})
/*
export class AppComponent {
  title = 'TechAssignWebApp';

  urldev : string = "http://127.0.0.1:3002/devices"; 

  constructor(private http: HttpClient){
  }
  httpdata;
  ngOnInit() {
    //
    this.http.get("http://127.0.0.1:3002/devices").subscribe((data) => this.displaydata(data));
  }
  displaydata(data) {this.httpdata = data;}

}
*/

@Component({
   selector: 'app-day-cmp',
   templateUrl: './day-cmp.component.html',
   styleUrls: ['./day-cmp.component.css']
})
export class DayComponent implements OnInit {
   daycomponent = "Entered in Day Summary component";
  
   constructor(private http: HttpClient){
   }
   httpdata;
   ngOnInit() {
    //
    this.http.get("http://127.0.0.1:3002/devices/day").subscribe((data) => this.displaydata(data));
  }
  displaydata(data) {this.httpdata = data;}
}