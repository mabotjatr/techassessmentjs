import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
/*
export class AppComponent {
  title = 'TechAssignWebApp';

  urldev : string = "http://127.0.0.1:3002/devices";

  constructor(private http: HttpClient){
  }
  httpdata;
  ngOnInit() {
    //
    this.http.get("http://127.0.0.1:3002/devices").subscribe((data) => this.displaydata(data));
  }
  displaydata(data) {this.httpdata = data;}

}
*/

@Component({
   selector: 'app-week-cmp',
   templateUrl: './week-cmp.component.html',
   styleUrls: ['./week-cmp.component.css']
})
export class WeekComponent implements OnInit {
   weekcomponent = "Entered in week Summary component";


   constructor(private http: HttpClient){
   }
   httpdata;
   ngOnInit() {
    //
    this.http.get("http://127.0.0.1:3002/devices/week").subscribe((data) => this.displaydata(data));
  }
  displaydata(data) {this.httpdata = data;}
}