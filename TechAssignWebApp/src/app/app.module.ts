import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from 'selenium-webdriver/http'; //@angular/common/http//selenium-webdriver/http
import { RouterModule } from '@angular/router';
//import { WeekRouterModule } from '@angular/router';
import { DayComponent } from './day-cmp.component';
import { WeekComponent } from './week-cmp.component';
/*import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';*/

@NgModule({
  declarations: [
    AppComponent,
    DayComponent,
    WeekComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, //
    AppRoutingModule,
    HttpClientModule,
    //WeekRouterModule,
    RouterModule.forRoot([
      {
         path: 'day-cmp',
         component: DayComponent,
      }
   ])
   /*, WeekRouterModule.forRoot([
    {
       path: 'week-cmp',
       component: WeekComponent,
    }
 ])*/
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
