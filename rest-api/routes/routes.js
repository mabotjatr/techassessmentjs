// Load the MySQL pool connection
const pool = require('../data/config');

// Route the app
const router = app => {
    // Display welcome message on the root
    app.get('/', (request, response) => {
        response.send({
            message: 'Welcome to the Node.js Express REST API!'
        });
    });

    // Display all devices
    app.get('/devices', (request, response) => {
        pool.query('SELECT * FROM raw_data', (error, result) => {
            if (error) throw error;

            response.send(result);
        });
    });

    // Display a single device by mac
    app.get('/devices/:mac', (request, response) => {
        const mac = request.params.mac;

        pool.query('SELECT * FROM raw_data WHERE device_mac = ?', mac, (error, result) => {
            if (error) throw error;

            response.send(result);
        });
    });
	
	// Display a  devices by day
    app.get('/devices/day', (request, response) => {
        const mac = request.params.mac;

        pool.query('SELECT * FROM raw_data WHERE date >= DATE_SUB(CURDATE(), INTERVAL 0 DAY)', mac, (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    });
	
	// Display a  devices by week
    app.get('/devices/week', (request, response) => {
        const mac = request.params.mac;

        pool.query('SELECT * FROM raw_data WHERE date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)', mac, (error, result) => {
            if (error) throw error;
            response.send(result);
        });
    });
}

// Export the router
module.exports = router;
